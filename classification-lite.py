#Real time reading of Myo Data 

print('Importing libraries...')
from MyoBand.common import *
from MyoBand.myo_raw import MyoRaw
from tensorflow.keras.models import load_model
from collections import deque
import time
import numpy as np
import threading

class GestureRecognizer(MyoRaw):
    predict_interval = 25
    gestures = [('Clench Fist', 0.7), ('Spider Man', 0.6),
                ('Thumb to Pinky', 0.7), ('Wrist Side to Side Horizontal', 0.7),
                ('Wrist Up and Down', 0.95), ('Wrist Rotate Inwards', 0.7),
                ('Wrist Side to Side Vertical', 0.7), ('Pointer Finger', 0.7)]

    def __init__(self, model):
        MyoRaw.__init__(self, None)
        self.add_emg_handler(self.emg_handler)
        self.connect()
        self.model = model
        size = model.layers[0].input_shape[1]
        self.window = deque([tuple([0] * 8)] * size, size)
        self.queued = 0
        
    def emg_handler(self, emg, _):
        print('.', flush=True, end='')
        self.window.append(emg)
        
        if self.queued == self.predict_interval:
            threading.Thread(target=self.predict()).start()
            self.queued = 0
        else:
            self.queued += 1
    
    def predict(self):
        print('\nPredicting...')
        features = np.array([np.vstack(np.array(self.window))])
        prediction = model.predict(features, use_multiprocessing=True, verbose=0)[0]
        (gesture, threshold) = self.gestures[np.argmax(prediction)]
        if max(prediction) > threshold:
            print('Gesture: {}, Confidence: {:.2f}/{:.2f}'.format(gesture, max(prediction), threshold))

print('Loading model...')
model = load_model('model.tf')
print('Connecting to the MyoBand...')
gr = GestureRecognizer(model)
print('Waiting for gestures')
while True:
    try:
        gr.run()
    except struct.error:
        pass

#0,150,8 REMEMBER
