from tensorflow.keras.preprocessing.sequence import pad_sequences
from statistics import median
import pandas as pd
import numpy as np
import os
import re

def load_emg_from_folder(path):
    all_gestures = [read_repeats(os.path.join(path, file), matches.groups()[0])
                    for file in os.listdir(path)
                    if (matches := re.match(r'emgData-G(\d).csv', file))]
    return pd.concat(all_gestures, ignore_index=True)

def read_repeats(file, gesture):
    df = pd.read_csv(file).loc[:, 'emg0':'emg7']
    bounds = df[np.isnan(df['emg1'])].index.insert(0,-1)
    repeats = [df.iloc[start+1:end].to_numpy(dtype='int32')
               for start,end in zip(bounds, bounds[1:])]
    return pd.DataFrame({'emg': repeats, 'gesture': gesture})

def shape_ml_data(df):
    middle = int(median([x.shape[0] for x in df['emg']]))
    data = np.stack([pad_sequences(x.T, maxlen = middle).T for x in df['emg']])
    labels = pd.get_dummies(df['gesture']).values
    return (data, labels)