from preprocessing import load_emg_from_folder, pad_emg_data
import matplotlib.pyplot as plt
import numpy as np

data = load_emg_from_folder('data/final/')
data = pad_emg_data(data)

def plot_emg_for_gesture(n):
    emg = data.loc[data[f'gesture_{n}'] == 1]['emg']
    plt.rc('font', size=24)
    plt.figure(figsize=(18,12))
    for ch in range(8):
        ch_avg = []
        for repeat in emg:
            ch_avg.append(repeat[ch])
        plt.plot(np.average(np.vstack(ch_avg), axis=0), label = f'Ch {ch}')
    plt.legend()
    plt.title(f'Average EMG Pattern from Gesture {n}')
    plt.xlabel('Time')
    plt.ylabel('Activation')

for i in range(data.shape[1] - 1):
    plot_emg_for_gesture(i)
