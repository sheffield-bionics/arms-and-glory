import gi
import visualise
import keyboard
import csv

import pandas as pd

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import os
import shutil

message_deleted_gestures = list()
message_new_gestures = list()

for root, dirs, files in os.walk('data/final/pngplots'):
    for f in files:
        os.unlink(os.path.join(root, f))
    for d in dirs:
        shutil.rmtree(os.path.join(root, d))

try:
    gestures = pd.read_csv("gestures.csv")
    gestures = gestures.values.tolist()
    gesture_length = len(gestures)
except:
    gestures = []
    gesture_length = len(gestures)


def index_2d(myList, v):
    for i, x in enumerate(myList):
        if v in x:
            return (i, x.index(v))

class Main:
    def __init__(self):
        glade_file = "UpdatedGUI.glade"
        self.builder = Gtk.Builder()
        self.builder.add_from_file(glade_file)
        self.builder.connect_signals(self)

        # Creation of the buttons

        new_gesture = self.builder.get_object("NewGesture_btn")
        new_gesture.connect("clicked", self.New_Gesture)

        add_btn = self.builder.get_object("Add_btn")
        add_btn.connect("clicked", self.Add_Repeat)

        remove_btn = self.builder.get_object("Remove_btn")
        remove_btn.connect("clicked", self.Remove_Repeat)

        record_btn = self.builder.get_object("Record_btn")
        record_btn.connect("clicked", self.Record)

        play_btn = self.builder.get_object("Play_btn")
        play_btn.connect("clicked", self.Play)

        save_btn = self.builder.get_object("Save_btn")
        save_btn.connect("clicked", self.Save)

        save_as_btn = self.builder.get_object("save_gestures")
        save_as_btn.connect("clicked", self.Save_Gestures)

        delete_btn = self.builder.get_object("Delete_btn")
        delete_btn.connect("clicked", self.Delete_Gesture)

        plot_btn = self.builder.get_object("Plot_btn")
        plot_btn.connect("clicked", self.Plot)

    # Define the gesture numbers and hide them until they are named

        radio_1 = self.builder.get_object("radio_1")
        radio_1.hide()

        radio_2 = self.builder.get_object("radio_2")
        radio_2.hide()

        radio_3 = self.builder.get_object("radio_3")
        radio_3.hide()

        radio_4 = self.builder.get_object("radio_4")
        radio_4.hide()

        radio_5 = self.builder.get_object("radio_5")
        radio_5.hide()

        radio_6 = self.builder.get_object("radio_6")
        radio_6.hide()

        radio_7 = self.builder.get_object("radio_7")
        radio_7.hide()

        radio_8 = self.builder.get_object("radio_8")
        radio_8.hide()

        radio_9 = self.builder.get_object("radio_9")
        radio_9.hide()

        radio_10 = self.builder.get_object("radio_10")
        radio_10.hide()

        radio_11 = self.builder.get_object("radio_11")
        radio_11.hide()

        radio_12 = self.builder.get_object("radio_12")
        radio_12.hide()

        radio_13 = self.builder.get_object("radio_13")
        radio_13.hide()

        #for i in range(len(gestures)):
        #    label = self.Label(label=f"Gesture{i+1}")
        #    Gtk.label.set_text(gestures[i][0])
        for number_of_gesture in range(0,len(gestures)):
            number_of_gestures = number_of_gesture + 1
            iteration = self.builder.get_object(f"Gesture{number_of_gestures}")
            iteration.set_text(gestures[number_of_gesture][0])
            radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
            radio_number.show()

    # Creating the window
        window = self.builder.get_object("Window1")
        window.connect("destroy", Gtk.main_quit)
        window.show()


    # Defining the functions for each of the buttons
    def New_Gesture(self, widget):  # Sets an entry for each of the elements in the gesture list and displays them
        if len(gestures) == 12:
            dialogWindow = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR,
            Gtk.ButtonsType.OK, "You have exceeded the maximum "+
            "number of gestures. Delete one of the existing ones to proceed")

            dialogWindow.set_title("Error")

            dialogWindow.show_all()
            response = dialogWindow.run()
            dialogWindow.destroy()

        else:
            dialogWindow = Gtk.MessageDialog(None,
                          Gtk.DialogFlags.DESTROY_WITH_PARENT,
                          Gtk.MessageType.QUESTION,
                          Gtk.ButtonsType.OK,
                          text = "Enter the name for the New Gesture")

            dialogWindow.set_title("New Gesture")

            dialogBox = dialogWindow.get_content_area()
            userEntry = Gtk.Entry()
            userEntry.set_size_request(250,0)
            dialogBox.pack_end(userEntry, False, False, 0)

            dialogWindow.show_all()
            response = dialogWindow.run()
            gesture_name = userEntry.get_text()
            dialogWindow.destroy()
            if (response == Gtk.ResponseType.OK):
                add = [gesture_name, 0.1]
                gestures.append(add)
                global message_new_gestures
                message_new_gestures.append(gesture_name)
                for number_of_gesture in range(0,len(gestures)):
                    number_of_gestures = number_of_gesture + 1
                    iteration = self.builder.get_object(f"Gesture{number_of_gestures}")
                    iteration.set_text(gestures[number_of_gesture][0])
                    radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
                    radio_number.show()
            else:
                return None


    def Add_Repeat(self, widget):
        print("New gesture added")

    def Remove_Repeat(self, widget):
        print("Gesture removed")

    def Record(self, widget):
        print("Recording started")

    def Play(self, widget):
        print("Recording started playing")

    def Save(self, widget):
        print("Saved")

    def Save_Gestures(self, widget):
        dialogWindow = Gtk.MessageDialog(None, 0, Gtk.MessageType.WARNING,
        Gtk.ButtonsType.OK_CANCEL,
        "Are you sure you want to save the current Gestures?")

        global message_new_gestures
        global message_deleted_gestures
        out = []
        out.append("The following Gestures will be Added: \n")
        for i in range(len(message_new_gestures)):
            out.append("    ")
            out.append(message_new_gestures[i])
            out.append("\n")

        out.append("\nThe following Gestures will be Deleted: \n")
        for i in range(len(message_deleted_gestures)):
            out.append("    ")
            out.append(message_deleted_gestures[i])
            out.append("\n")

        dialogWindow.format_secondary_text(''.join(out))

        dialogWindow.set_title("Save Gestures")

        dialogWindow.show_all()
        response = dialogWindow.run()
        dialogWindow.destroy()

        if (response == Gtk.ResponseType.OK):
            export_data = [['NaN', 0]]
            for i in range(len(gestures)):
                export_data.append(gestures[i][:])
            data_out = pd.DataFrame(export_data)
            data_out.to_csv('gestures.csv', index = False, header = False)

            dialogWindow = Gtk.MessageDialog(None, 0, Gtk.MessageType.WARNING,
            Gtk.ButtonsType.OK,
            "The Gestures have been saved as requested")

            dialogWindow.set_title("Gestures Saved")

            dialogWindow.show_all()
            response = dialogWindow.run()
            dialogWindow.destroy()
            message_new_gestures = []
            message_deleted_gestures = []
        elif (response == Gtk.ResponseType.CANCEL):
            dialogWindow = Gtk.MessageDialog(None, 0, Gtk.MessageType.WARNING,
            Gtk.ButtonsType.OK,
            "The Gestures have not been saved as requested")

            dialogWindow.set_title("Gestures Not Saved")

            dialogWindow.show_all()
            response = dialogWindow.run()
            dialogWindow.destroy()



    def Delete_Gesture(self, widget):   # Finds a specific gesture in the gesture list and removes it
        for length in range(len(gestures)):
            indexes = length + 1
            radio_number = self.builder.get_object(f"radio_{indexes}")
            if radio_number.get_active():
                gesture_name = self.builder.get_object(f"Gesture{indexes}").get_text()
                #position = gestures.index(gesture_name)
                position = index_2d(gestures, gesture_name)
                position = int(position[0])
                for number_of_gesture in range(len(gestures)):
                    if number_of_gesture < position:  # Basically works in the same way as "New Gesture"
                        number_of_gestures = number_of_gesture + 1
                        iteration = self.builder.get_object(f"Gesture{number_of_gestures}")
                        iteration.set_text(gestures[number_of_gesture][0])
                        radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
                        radio_number.show()
                    elif number_of_gesture == position:  # Hides the entry that has the gesture that is being removed
                        number_of_gestures = number_of_gesture + 1
                        iteration = self.builder.get_object(f"Gesture{number_of_gestures}")
                        iteration.set_text("")
                        radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
                        radio_number.hide()
                    elif number_of_gesture > position:  # After the index of the targeted gesture has been passed,
                        # the process needs to be changed, as there will be one less entry to worry about. That is why
                        # this do not include the first "+1". In addition, the following entry is hidden so that it does not
                        # show the gestures that were previously set with the "New Gesture" button
                        iteration = self.builder.get_object(f"Gesture{number_of_gesture}")
                        iteration.set_text(gestures[number_of_gesture][0])
                        radio_number = self.builder.get_object(f"radio_{number_of_gesture}")
                        radio_number.show()
                        hide_next_iteration = self.builder.get_object(f"Gesture{number_of_gesture + 1}")
                        hide_next_iteration.set_text("")
                        hide_number = self.builder.get_object(f"radio_{number_of_gesture + 1}")
                        hide_number.hide()
                global message_deleted_gestures
                message_deleted_gestures.append(gestures[position][0])
                gestures.remove(gestures[position])


    def Plot(self, widget):
        for number_of_gesture in range(len(gestures)):
            number_of_gestures = number_of_gesture + 1
            radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
            if radio_number.get_active():
                try:
                    visualise.plot_emg_for_gesture(number_of_gesture)
                    img_disp = self.builder.get_object("img_disp")
                    img_disp.set_from_file("data/final/pngplots/" + f"plot{number_of_gesture}.png")
                except:
                    dialogWindow = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "The Data to Plot the Gesture does not exist")

                    dialogWindow.set_title("Error")

                    dialogWindow.show_all()
                    response = dialogWindow.run()
                    dialogWindow.destroy()

#############################################################################

if __name__ == '__main__':
    main = Main()
    Gtk.main()