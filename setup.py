from setuptools import setup


setup(name='arms-and-glory',
      version='1.0',
      description='Get swoll with yer arm',
      url='http://gitlab.com/sheffield-bionics/arms-and-glory',
      author='Sheffield Bionics',
      author_email='bionics@sheffield.ac.uk',
      license='GPL3',
      scripts=[
          'Classificaion.py',
          'collection.py',
          'neural.py',
          'preprocessing.py',
          'visualise.py',
      ],
      install_requires=[
          'tensorflow',
          'scipy',
          'scikit-learn',
          'matplotlib',
          'statistics',
          'pandas',
          'numpy',
          'pynput',
          'pyserial',
          'cython',
      ],
      zip_safe=True)
