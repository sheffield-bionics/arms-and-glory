from preprocessing import load_emg_from_folder, pad_emg_data
from math import ceil, sqrt
import matplotlib.pyplot as plt
import numpy as np

data = load_emg_from_folder('data/final/')
data = pad_emg_data(data)

def plot_emg_for_gesture(cell, n):
    emg = data.loc[data[f'gesture_{n}'] == 1]['emg']
    for ch in range(8):
        ch_avg = []
        for repeat in emg:
            ch_avg.append(repeat[ch])
        cell.plot(np.average(np.vstack(ch_avg), axis=0), label = f'Ch {ch}')
    cell.set_title(f'Gesture {n}')
    cell.set_xlabel('Time')
    cell.set_ylabel('Activation')

plt.rc('font', size=24)
gestures = data.shape[1] - 1
sz = ceil(sqrt(gestures))
fig, grid = plt.subplots(sz,sz)
fig.set_size_inches(18,12)
fig.suptitle('Average EMG Patterns')
for i in range(sz**2):
    cell = grid[i // sz][i % sz]
    if i < gestures:
        plot_emg_for_gesture(cell, i)
    cell.set_axis_off()