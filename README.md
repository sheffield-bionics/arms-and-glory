# Arms and Glory

## Install

```bash
python setup.py develop --user
```

## Virtual Environment Setup
```bash
# First-time setup
python -m venv .venv

# Activate (for fish)
source .venv/bin/activate.fish

# Upgrade pip and install packages
pip install --upgrade pip
pip install -r requirements.txt

# Deactivate virtual environment
deactivate
```