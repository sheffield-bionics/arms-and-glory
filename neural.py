from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, GRU, Conv1D, GlobalMaxPooling1D
from tensorflow.math import argmax
from preprocessing import load_emg_from_folder, shape_ml_data
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from statistics import mean, stdev
import matplotlib.pyplot as plt
import tensorflow.lite as tflite
import time

data = load_emg_from_folder('data/final/')
emg_tensor, labels = shape_ml_data(data)

# FIXME: Try using fewer features (not 128), but more layers
def build_cnn_model(classes):
    model = Sequential()
    model.add(Conv1D(128, 1, activation='relu'))
    model.add(Conv1D(64, 3, padding='same', activation='relu'))
    model.add(Conv1D(32, 3, strides=2, padding='same', activation='relu'))
    model.add(GlobalMaxPooling1D())
    model.add(Dense(classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def build_lstm_model(classes):
    model = Sequential()
    # An activation='relu' leads to divergence and the model never improving
    model.add(LSTM(64))
    model.add(Dense(classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def build_gru_model(classes):
    model = Sequential()
    model.add(GRU(64))
    model.add(Dense(classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def train_model(model, x_data, y_data, repeats=15, epochs=30):
    model.build((None, *x_data.shape[1:]))
    init_weights = model.get_weights()
    results = {}
    times = []
    for rep in range(repeats):
        x_train, x_test, y_train, y_test = train_test_split(x_data, y_data)
        results[rep] = []
        model.set_weights(init_weights)
        start = time.monotonic()
        for e in range(epochs+1):
            model.fit(x_train, y_train, epochs=1, verbose=0)
            results[rep].append(model.evaluate(x_test, y_test, batch_size=1, verbose=0)[1])
            print(f'Repeat: {rep}/{repeats}; Epoch: {e}/{epochs}; Accuracy: {results[rep][-1] * 100:.1f}%')
        end = time.monotonic()
        times.append(end - start)
    return (model, results, times)

def evaluate(model, x_data, y_data):
    y_pred = model.predict(x_data, verbose=0)
    print(confusion_matrix(argmax(y_data, axis=1), argmax(y_pred, axis=1)))
    acc = model.evaluate(x_data, y_data, verbose=0)[1] * 100
    print('Accuracy: {:.1f}%'.format(acc))

def plot_results(results):
    plt.rc('font', size=24)
    plt.figure(figsize=(18,12))
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    for arr in results.values():
        plt.plot(arr)

def summarise_results(results):
    xs = [x[-1] for x in results.values()]
    print(f'Average Accuracy: {mean(xs)} +/- {stdev(xs)}')

def save_best_model(x_data, y_data, repeats=15):
    best = (0, None)
    for n in range(repeats):
        model = build_cnn_model(y_data.shape[1])
        x_train, x_test, y_train, y_test = train_test_split(x_data, y_data)
        model.fit(x_train, y_train, epochs=30)
        acc = model.evaluate(x_data, y_data)[1]
        if acc > best[0]:
            best = (acc, model)
    print(f'Best was {best[0] * 100}%')
    best[1].save('model.tf')
    lite_model = tflite.TFLiteConverter.from_keras_model(best[1])
    open("model.tflite", "wb").write(lite_model.convert())


#%%

model = build_cnn_model(labels.shape[1])
(model, results, times) = train_model(model, emg_tensor, labels)
model.summary()
plot_results(results)
evaluate(model, emg_tensor, labels)
print(f'Average time to train: {mean(times):.1f}s')
summarise_results(results)

#%%

# Final Comparison (Desktop + 100 Repeats)

# _________________________________________________________________
# Layer (type)                 Output Shape              Param #   
# =================================================================
# lstm (LSTM)                  (None, 128)               70144     
# _________________________________________________________________
# dense_1 (Dense)              (None, 8)                 1032      
# =================================================================
# Total params: 71,176
# Trainable params: 71,176
# Non-trainable params: 0
# _________________________________________________________________
# [[30  0  0  0  0  0  0  0]
#  [ 1 29  0  0  0  0  0  0]
#  [ 0  0 29  0  1  0  0  0]
#  [ 0  0  0 28  1  0  0  1]
#  [ 0  0  0  0 30  0  0  0]
#  [ 0  0  0  0  0 30  0  0]
#  [ 0  0  0  0  0  1 20  0]
#  [ 0  0  0  0  0  0  0 30]]
# Accuracy: 97.8%
# Average time to train: 30.5s
# Average Accuracy: 0.9741379350423813 +/- 0.02212321555642708

# _________________________________________________________________
# Layer (type)                 Output Shape              Param #   
# =================================================================
# gru_2 (GRU)                  (None, 64)                14208     
# _________________________________________________________________
# dense_19 (Dense)             (None, 8)                 520       
# =================================================================
# Total params: 14,728
# Trainable params: 14,728
# Non-trainable params: 0
# _________________________________________________________________
# [[30  0  0  0  0  0  0  0]
#  [ 0 30  0  0  0  0  0  0]
#  [ 0  0 30  0  0  0  0  0]
#  [ 0  0  0 30  0  0  0  0]
#  [ 0  0  0  0 30  0  0  0]
#  [ 0  1  0  0  0 29  0  0]
#  [ 0  0  0  0  0  0 21  0]
#  [ 0  0  0  0  0  0  0 30]]
# Accuracy: 99.6%
# Average time to train: 19.0s
# Average Accuracy: 0.9598275899887085 +/- 0.02696253859522573

# _________________________________________________________________
# Layer (type)                 Output Shape              Param #   
# =================================================================
# conv1d (Conv1D)              (None, 118, 128)          1152      
# _________________________________________________________________
# conv1d_1 (Conv1D)            (None, 116, 64)           24640     
# _________________________________________________________________
# conv1d_2 (Conv1D)            (None, 57, 32)            6176      
# _________________________________________________________________
# global_max_pooling1d (Global (None, 32)                0         
# _________________________________________________________________
# dense (Dense)                (None, 8)                 264       
# =================================================================
# Total params: 32,232
# Trainable params: 32,232
# Non-trainable params: 0
# _________________________________________________________________
# [[30  0  0  0  0  0  0  0]
#  [ 0 30  0  0  0  0  0  0]
#  [ 0  0 29  0  0  1  0  0]
#  [ 0  0  0 30  0  0  0  0]
#  [ 0  0  0  0 30  0  0  0]
#  [ 0  0  0  0  0 30  0  0]
#  [ 0  0  0  0  0  0 21  0]
#  [ 0  0  0  0  0  0  0 30]]
# Accuracy: 99.6%
# Average time to train: 3.1s
# Average Accuracy: 0.9832758677005767 +/- 0.019210912687503093

# _________________________________________________________________
# Layer (type)                 Output Shape              Param #   
# =================================================================
# conv1d_12 (Conv1D)           (None, 200, 128)          1152      
# _________________________________________________________________
# conv1d_13 (Conv1D)           (None, 198, 64)           24640     
# _________________________________________________________________
# conv1d_14 (Conv1D)           (None, 98, 32)            6176      
# _________________________________________________________________
# global_max_pooling1d_4 (Glob (None, 32)                0         
# _________________________________________________________________
# dense_5 (Dense)              (None, 7)                 231       
# =================================================================
# Total params: 32,199
# Trainable params: 32,199
# Non-trainable params: 0
# _________________________________________________________________
# [[10  0  0  0  0  0  0]
#  [ 0 11  0  0  0  0  0]
#  [ 1  1  9  1  0  1  0]
#  [ 0  0  0 10  0  1  0]
#  [ 0  0  0  1 11  0  0]
#  [ 0  2  1  0  0  7  0]
#  [ 0  1  0  0  0  0  4]]
# Accuracy: 86.1%
# Average time to train: 6.4s
# Average Accuracy: 0.6594444498419761 +/- 0.11809029029562539
# Comments: 100 epochs, Mark's data, 38.9% - 94.4%

# Final Comparison:

# _________________________________________________________________
# Layer (type)                 Output Shape              Param #   
# =================================================================
# lstm (LSTM)                  (None, 128)               70144     
# _________________________________________________________________
# dense (Dense)                (None, 8)                 1032      
# =================================================================
# Total params: 71,176
# Trainable params: 71,176
# Non-trainable params: 0
# _________________________________________________________________
# [[29  0  0  0  1  0  0  0]
#  [ 0 30  0  0  0  0  0  0]
#  [ 0  0 30  0  0  0  0  0]
#  [ 0  0  0 30  0  0  0  0]
#  [ 0  0  0  0 30  0  0  0]
#  [ 0  0  1  0  0 29  0  0]
#  [ 0  0  0  0  0  0 21  0]
#  [ 0  0  0  0  0  0  0 30]]
# Accuracy: 99.1%
# Average time to train: 27.7s
# Average Accuracy: 0.9563218355178833 +/- 0.031836027599966135

# _________________________________________________________________
# Layer (type)                 Output Shape              Param #   
# =================================================================
# conv1d (Conv1D)              (None, 118, 128)          1152      
# _________________________________________________________________
# conv1d_1 (Conv1D)            (None, 116, 64)           24640     
# _________________________________________________________________
# conv1d_2 (Conv1D)            (None, 57, 32)            6176      
# _________________________________________________________________
# global_max_pooling1d (Global (None, 32)                0         
# _________________________________________________________________
# dense_1 (Dense)              (None, 8)                 264       
# =================================================================
# Total params: 32,232
# Trainable params: 32,232
# Non-trainable params: 0
# _________________________________________________________________
# [[30  0  0  0  0  0  0  0]
#  [ 0 30  0  0  0  0  0  0]
#  [ 0  0 30  0  0  0  0  0]
#  [ 0  0  0 30  0  0  0  0]
#  [ 0  0  0  0 30  0  0  0]
#  [ 0  0  0  0  0 30  0  0]
#  [ 0  0  0  0  0  0 21  0]
#  [ 0  0  0  0  0  0  0 30]]
# Accuracy: 100.0%
# Average time to train: 4.0s
# Average Accuracy: 0.9643678228060405 +/- 0.026443914502488102

# Laptop Testing:

# Conv1D(8, 1) + Flatten + Dense
#  Average time to train: 4.9s
# [0.8965517282485962, 0.8620689511299133, 0.931034505367279, 0.8965517282485962, 0.9655172228813171]
# 0.9103448271751404 +/- 0.03931639414920386
# Comments: Spacial information comes from the Dense network at the end

# Conv1D(8, 1) + GlobalMaxPooling1D + Dense
# Average time to train: 1.9s
# [0.3103448152542114, 0.32758620381355286, 0.32758620381355286, 0.2931034564971924, 0.37931033968925476]
# 0.32758620381355286 +/- 0.0322556645181363
# Comments: Spacial information is discarded

# Conv1D(128, 1) + Conv1D(64, 3) + Conv1D(64, 3, strides=2) + GlobalMaxPooling1D + Dense
# Average time to train: 3.8s
# [0.9655172228813171, 0.982758641242981, 0.9655172228813171, 1.0, 0.982758641242981, 0.9482758641242981, 0.9655172228813171, 0.9482758641242981, 0.9655172228813171, 0.982758641242981, 1.0, 1.0, 1.0, 0.982758641242981, 0.982758641242981, 1.0, 1.0, 0.9482758641242981, 1.0, 0.9655172228813171, 0.982758641242981, 0.982758641242981, 0.9482758641242981, 0.9655172228813171, 0.9482758641242981, 0.982758641242981, 0.982758641242981, 0.982758641242981, 1.0, 0.982758641242981, 1.0, 0.9655172228813171, 0.9655172228813171, 1.0, 0.9655172228813171, 0.982758641242981, 0.982758641242981, 1.0, 0.982758641242981, 0.9482758641242981]
# 0.9784482792019844 +/- 0.017785409032507134

# Conv1D(128, 1) + Conv1D(64, 3) + Conv1D(32, 3, strides=2) + GlobalMaxPooling1D + Dense
# Average time to train: 3.6s
# [0.9655172228813171, 0.9482758641242981, 0.982758641242981, 0.982758641242981, 0.9482758641242981, 0.982758641242981, 1.0, 0.9655172228813171, 1.0, 0.982758641242981, 0.982758641242981, 0.982758641242981, 0.982758641242981, 1.0, 1.0, 0.982758641242981, 0.982758641242981, 1.0, 0.982758641242981, 1.0, 1.0, 0.982758641242981, 0.9655172228813171, 1.0, 1.0, 1.0, 0.982758641242981, 1.0, 1.0, 0.9655172228813171, 0.9482758641242981, 0.982758641242981, 0.9482758641242981, 0.9655172228813171, 1.0, 0.9655172228813171, 0.982758641242981, 1.0, 1.0, 0.982758641242981]
# 0.9831896603107453 +/- 0.01678780264889458

# 5 Repeats:

# LSTM(256)
# Average time to train: 40.0s (I could halve this, not much movement after 15 epochs)
# [1.0, 0.9655172228813171, 0.982758641242981, 0.9655172228813171, 0.982758641242981]
# 0.9793103456497192 +/- 0.014425184171610537

# LSTM(128)
# Average time to train: 31.7s
# [1.0, 0.9482758641242981, 0.9482758641242981, 0.931034505367279, 0.931034505367279]
# 0.9517241477966308 +/- 0.028330468731738993

# LSTM(64)
# Average time to train: 21.3s
# [0.8965517282485962, 0.982758641242981, 1.0, 0.8965517282485962, 0.9137930870056152]
# 0.9379310369491577 +/- 0.04967193696011894

# LSTM(32)
# Average time to train: 18.2s
# [0.7931034564971924, 0.7586206793785095, 0.7758620977401733, 0.6034482717514038, 0.7241379022598267]
# 0.7310344815254212 +/- 0.07574443027642264

# 25 Repeats:

# Mono
# [0.982758641242981, 0.982758641242981, 0.9655172228813171, 0.982758641242981, 0.982758641242981, 0.9655172228813171, 0.9655172228813171, 0.9482758641242981, 1.0, 0.9655172228813171, 0.9655172228813171, 0.9655172228813171, 0.9655172228813171, 0.9655172228813171, 0.9482758641242981, 0.9655172228813171, 0.982758641242981, 0.9655172228813171, 0.8965517282485962, 0.931034505367279, 0.982758641242981, 0.982758641242981, 1.0, 0.931034505367279, 0.9137930870056152]
# 0.9641379308700562 +/- 0.02484594066784774

# Hybrid
# [1.0, 0.9482758641242981, 0.982758641242981, 1.0, 0.9482758641242981, 0.9655172228813171, 0.9482758641242981, 0.9655172228813171, 0.982758641242981, 0.982758641242981, 0.931034505367279, 0.982758641242981, 0.9655172228813171, 0.982758641242981, 0.9655172228813171, 0.982758641242981, 0.9655172228813171, 0.9655172228813171, 0.982758641242981, 0.9655172228813171, 0.982758641242981, 0.982758641242981, 0.982758641242981, 1.0, 0.982758641242981]
# 0.9737931084632874 +/- 0.017327376508676565

# LSTM(140)
# [0.9482758641242981, 0.931034505367279, 1.0, 0.982758641242981, 0.982758641242981, 0.9482758641242981, 0.9655172228813171, 0.982758641242981, 0.931034505367279, 0.9655172228813171, 1.0, 1.0, 0.982758641242981, 0.9655172228813171, 0.9655172228813171, 0.9482758641242981, 1.0, 1.0, 0.9655172228813171, 0.931034505367279, 0.9655172228813171, 0.9137930870056152, 1.0, 1.0, 1.0]
# 0.9710344839096069 +/- 0.026673106109413088