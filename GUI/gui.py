#User interface

import gi
import visualise
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

gestures = []


class Main:
    def __init__(self):
        glade_file = "GUI.glade"
        self.builder = Gtk.Builder()
        self.builder.add_from_file(glade_file)
        self.builder.connect_signals(self)

    # Creation of the buttons

        new_gesture = self.builder.get_object("NewGesture_btn")
        new_gesture.connect("clicked", self.New_Gesture)

        add_btn = self.builder.get_object("Add_btn")
        add_btn.connect("clicked", self.Add_Repeat)

        remove_btn = self.builder.get_object("Remove_btn")
        remove_btn.connect("clicked", self.Remove_Repeat)

        record_btn = self.builder.get_object("Record_btn")
        record_btn.connect("clicked", self.Record)

        play_btn = self.builder.get_object("Play_btn")
        play_btn.connect("clicked", self.Play)

        save_btn = self.builder.get_object("Save_btn")
        save_btn.connect("clicked", self.Save)

        save_as_btn = self.builder.get_object("SaveAs_btn")
        save_as_btn.connect("clicked", self.Save_As)

        delete_btn = self.builder.get_object("Delete_btn")
        delete_btn.connect("clicked", self.Delete_Gesture)

        plot_btn = self.builder.get_object("Plot_btn")
        plot_btn.connect("clicked", self.Plot)

    # Define the gesture numbers and hide them until they are named

        radio_1 = self.builder.get_object("radio_1")
        radio_1.hide()

        radio_2 = self.builder.get_object("radio_2")
        radio_2.hide()

        radio_3 = self.builder.get_object("radio_3")
        radio_3.hide()

        radio_4 = self.builder.get_object("radio_4")
        radio_4.hide()

        radio_5 = self.builder.get_object("radio_5")
        radio_5.hide()

        radio_6 = self.builder.get_object("radio_6")
        radio_6.hide()

        radio_7 = self.builder.get_object("radio_7")
        radio_7.hide()

        radio_8 = self.builder.get_object("radio_8")
        radio_8.hide()

        radio_9 = self.builder.get_object("radio_9")
        radio_9.hide()

        radio_10 = self.builder.get_object("radio_10")
        radio_10.hide()

        radio_11 = self.builder.get_object("radio_11")
        radio_11.hide()

        radio_12 = self.builder.get_object("radio_12")
        radio_12.hide()

        radio_13 = self.builder.get_object("radio_13")
        radio_13.hide()

    # Creating the window
        window = self.builder.get_object("Window1")
        window.connect("destroy", Gtk.main_quit)
        window.show()

    # Defining the functions for each of the buttons

    def New_Gesture(self, widget):  # Sets an entry for each of the elements in the gesture list and displays them
        if len(gestures) == 13:
            print("You have exceeded the maximum number of gestures. Delete one of the existing ones to proceed")
        gesture_name = input('Gesture name: ')
        gestures.append(gesture_name)
        for number_of_gesture in range(len(gestures)):
            number_of_gestures = number_of_gesture + 1
            iteration = self.builder.get_object(f"Gesture{number_of_gestures}")
            iteration.set_text(gestures[number_of_gesture])
            radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
            radio_number.show()

    def Add_Repeat(self, widget):
        print("New gesture added")

    def Remove_Repeat(self, widget):
        print("Gesture removed")

    def Record(self, widget):
        print("Recording started")

    def Play(self, widget):
        print("Recording started playing")

    def Save(self, widget):
        print("Saved")

    def Save_As(self, widget):
        print("Saved As")

    def Delete_Gesture(self, widget):   # Finds a specific gesture in the gesture list and removes it
        gesture_name = input('Gesture name: ')
        if gesture_name in gestures:
            position = gestures.index(gesture_name)
            for number_of_gesture in range(len(gestures)):
                if number_of_gesture < position:    # Basically works in the same way as "New Gesture"
                    number_of_gestures = number_of_gesture + 1
                    iteration = self.builder.get_object(f"Gesture{number_of_gestures}")
                    iteration.set_text(gestures[number_of_gesture])
                    radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
                    radio_number.show()
                elif number_of_gesture == position:     # Hides the entry that has the gesture that is being removed
                    number_of_gestures = number_of_gesture + 1
                    iteration = self.builder.get_object(f"Gesture{number_of_gestures}")
                    iteration.set_text("")
                    radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
                    radio_number.hide()
                elif number_of_gesture > position:  # After the index of the targeted gesture has been passed,
                    # the process needs to be changed, as there will be one less entry to worry about. That is why
                    # this do not include the first "+1". In addition, the following entry is hidden so that it does not
                    # show the gestures that were previously set with the "New Gesture" button
                    iteration = self.builder.get_object(f"Gesture{number_of_gesture}")
                    iteration.set_text(gestures[number_of_gesture])
                    radio_number = self.builder.get_object(f"radio_{number_of_gesture}")
                    radio_number.show()
                    hide_next_iteration = self.builder.get_object(f"Gesture{number_of_gesture+1}")
                    hide_next_iteration.set_text("")
                    hide_number = self.builder.get_object(f"radio_{number_of_gesture+1}")
                    hide_number.hide()
            gestures.remove(gesture_name)
        else:
            print("That gesture has not been defined yet")

    def Plot(self, widget):
        for number_of_gesture in range(len(gestures)):
            number_of_gestures = number_of_gesture + 1
            radio_number = self.builder.get_object(f"radio_{number_of_gestures}")
            if radio_number.get_active():
                visualise.plot_emg_for_gesture(number_of_gesture)

        img_disp = self.builder.get_object("img_disp")
        img_disp.set_from_file("data/final/pngplots/plot.png")

#############################################################################

if __name__ == '__main__':
    main = Main()
    Gtk.main()