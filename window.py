from collections import deque

class RollingWindow:
    def __init__(self, size, init_val=0):
        self.idx = 0
        self.size = size
        self.data = [init_val] * size

    def push(self, item):
        self.data[self.idx] = item
        self.idx = (self.idx + 1) % self.size

    def peek(self):
        return self.data[self.idx:] + self.data[:self.idx]

class SlidingWindow:
    def __init__(self, size, init_val=0):
        self.size = size
        self.data = [init_val] * size

    def push(self, item):
        self.data.append(item)
        if len(self.data) > self.size:
            self.data.pop(0)

    def peek(self):
        return self.data

def drive_window(win):
    for x in range(1000):
        win.push(x)
        if x % 100 == 0:
            win.peek()

class DequeWindow:
    def __init__(self, size, init_val=0):
        self.data = deque([init_val] * size, size)

    def push(self, item):
        self.data.append(item)

    def peek(self):
        return self.data

def test_benchmark_rolling(benchmark):
    benchmark(drive_window, RollingWindow(200))

def test_benchmark_sliding(benchmark):
    benchmark(drive_window, SlidingWindow(200))

def test_benchmark_deque(benchmark):
    benchmark(drive_window, DequeWindow(200))
