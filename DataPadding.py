# -*- coding: utf-8 -*-
"""
Created on Wed Jul 29 11:31:52 2020

@author: Nicholas
"""
import numpy
import pandas as pd

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers



#--------------- Keras Method --------------------
#not sure if it works, still not fully sure how to examine the data

padded_data = tf.keras.preprocessing.sequence.pad_sequences(data['emg'], padding="post")




#------------- Other method ---------------
#The pad_it fucntion works with the input being a list. You can change how long
#you want the data to be and what want to pad it with.

#the pad_data function trys to take the DataFrame pull out the array in each cell,
#covert it to a list, use the pad_it fuction, convert it back into an array and
#then put it back into the DataFrame


def pad_it(List, endLen,PadWith):
    ListLen = len(List)
    
    if ListLen < endLen:
        dif= endLen-ListLen
        listofzeros = [PadWith] * dif
        List = List + listofzeros
    elif ListLen > endLen:
        List = List[0:endLen]
    return List

##Example of pad_it
#List = [[4,5,6],[1,2,3],[5,6,9],[7,4,9]]
#NewList = pad_it(List,6,[0,0,0])


def pad_data(df):
    for indx in range(len(df)):
        arr = df.iloc[indx]['emg']
        List = arr.tolist()
        result = pad_it(List,10,[0]*8)
        newarr = np.array(result)
        df.iloc[indx]['emg'] = newarr
    return List

PaddedData = pad_data(data)

